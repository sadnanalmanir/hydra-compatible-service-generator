package ca.unbsj.cbakerlab.sadi.services;

@SuppressWarnings("unused")
private static final class Vocab
{
	private static Model m_model = ModelFactory.createDefaultModel();
		
	public static final Property has_last_name = m_model.createProperty("http://localhost:8080/hydra-test-ontologies/domain-ontologies/HAI.owl#has_last_name");
	public static final Property has_first_name = m_model.createProperty("http://localhost:8080/hydra-test-ontologies/domain-ontologies/HAI.owl#has_first_name");
	public static final Resource Patient = m_model.createResource("http://localhost:8080/hydra-test-ontologies/domain-ontologies/HAI.owl#Patient");

}

