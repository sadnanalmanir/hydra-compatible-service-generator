package ca.unbsj.cbakerlab.owltotptp;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        String[] vals = {"--cnf",   "-c", "-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
        //String[] vals = {"-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
        OWL2TPTP.main(vals);
    }
}
