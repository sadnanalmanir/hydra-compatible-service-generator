package ca.unbsj.cbakerlab.owltotptp.parameters;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import ca.unbsj.cbakerlab.owltotptp.Converter;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.Functor;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.OWL2TPTPParameters;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.OtherReservedSymbols;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.URIToSymbols;

public class Parser {

        public static final String SCHEMA_FILE_DIR = "owltotptp/schema/";
        public static final String SCHEMA_FILE_NAME = "owl-to-tptp-syntax.xsd";
        public static final String JAXB_DIR = "ca.unbsj.cbakerlab.owltotptp.parameters.jaxb";
	public Parser() throws java.lang.Exception {

		JAXBContext jc = 
		    JAXBContext.newInstance(JAXB_DIR);
		_unmarshaller = jc.createUnmarshaller();
		
		SchemaFactory schemaFactory = 
		    SchemaFactory.
		    newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);

		
                
                URL schemaURL = this.getClass().getClassLoader().getResource(SCHEMA_FILE_DIR + SCHEMA_FILE_NAME);
                if(schemaURL == null){
                    System.out.println("Schema files not found.");
                }
                
                
		
		Schema schema = schemaFactory.newSchema(schemaURL);

		_unmarshaller.setSchema(schema);

	    } // Parser()

	
	   /** Tries to parse the parameters file and augments 
     *  the values in <code>parameters</code> correspondingly.
     *  @throws java.lang.Exception if the parameters file does not
     *          comply with the XML Schema for owl2tptp.Converter parameter
     *          files, or the contents of the file clash with some previous
     *          values in <code>parameters</code>.
     */
    public 
	void 
	parse(File file,Converter.Parameters parameters)
	throws java.lang.Exception {

	OWL2TPTPParameters param;

	try
	    {
		param = 
		    (OWL2TPTPParameters)_unmarshaller.unmarshal(file);
	    }
	catch (javax.xml.bind.UnmarshalException ex)
	    {
		throw 
		    new java.lang.Exception("Parameters file cannot be read: " +
					    ex);
	    };

	for (Object topLevelOption : param.getTopLevelOption())
	    {
		if (topLevelOption instanceof URIToSymbols)
		    {
			URIToSymbols map = (URIToSymbols)topLevelOption;
			
			String uri = map.getURI();

			// Functors that have been already assigned to this URI:
			Set<Converter.TPTPSymbolDescriptor> existingDescriptors = 
			    parameters.URIsToTPTPSymbols().get(uri);
			
			if (existingDescriptors == null)
			    {
				existingDescriptors = 
				    new HashSet<Converter.TPTPSymbolDescriptor>();
				parameters.URIsToTPTPSymbols().put(uri,
								   existingDescriptors);
			    };


			for (Functor f : map.getFunctor())
			    {
				Converter.TPTPSymbolDescriptor desc =
				    new Converter.TPTPSymbolDescriptor(f.isPredicate(),
								       f.getName(),
								       f.getArity().intValue());
				
				// Check if this descriptor conflicts with the previous mapping:

				for (Converter.TPTPSymbolDescriptor d : existingDescriptors)
				    {
					if (d.isPredicate() == desc.isPredicate() &&
					    d.arity() == desc.arity() &&
					    !d.name().equals(desc.name()))
					    throw new java.lang.Exception("Error: URI " + uri + 
									  " has conflicting mappings: " +
									  d + " and " + desc);
				    };
				
				existingDescriptors.add(desc);

			    }; // for (Functor f : map.getFunctor())
		    }
		else if (topLevelOption instanceof OtherReservedSymbols) 
		    {
			OtherReservedSymbols set = (OtherReservedSymbols)topLevelOption;
			
			for (Functor f : set.getFunctor())
			    {
				Converter.TPTPSymbolDescriptor desc =
				    new Converter.TPTPSymbolDescriptor(f.isPredicate(),
								       f.getName(),
								       f.getArity().intValue());
				parameters.otherReservedSymbols().add(desc);
				
			    }; // for (Functor f : set.getFunctor())
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.DataDomainPredicate) 
		    {
			if (parameters.dataDomainPredicate() == null)
			    {
				parameters.
				    setDataDomainPredicate(((OWL2TPTPParameters.DataDomainPredicate)
							    topLevelOption).
							   getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.DataDomainPredicate)
				      topLevelOption).
				    getName().equals(parameters.dataDomainPredicate()))
				    throw new java.lang.Exception("Error: data domain predicate id " + 
								  ((OWL2TPTPParameters.DataDomainPredicate)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.dataDomainPredicate());
			    };
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.EqualityPredicate) 
		    {
			if (parameters.equalityPredicate() == null)
			    {
				parameters.
				    setEqualityPredicate(((OWL2TPTPParameters.EqualityPredicate)
							    topLevelOption).
							   getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.EqualityPredicate)
				      topLevelOption).
				    getName().equals(parameters.equalityPredicate()))
				    throw new java.lang.Exception("Error: equality predicate id " + 
								  ((OWL2TPTPParameters.EqualityPredicate)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.equalityPredicate());
			    };
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.IntLitFunc) 
		    {
			if (parameters.intLitFunc() == null)
			    {
				parameters.
				    setIntLitFunc(((OWL2TPTPParameters.IntLitFunc)
						   topLevelOption).
						  getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.IntLitFunc)
				      topLevelOption).
				    getName().equals(parameters.intLitFunc()))
				    throw new java.lang.Exception("Error: integer-literal function " + 
								  ((OWL2TPTPParameters.IntLitFunc)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.intLitFunc());
			    };
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.StringLitNoLangFunc) 
		    {
			if (parameters.stringLitNoLangFunc() == null)
			    {
				parameters.
				    setStringLitNoLangFunc(((OWL2TPTPParameters.StringLitNoLangFunc)
							    topLevelOption).
							   getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.StringLitNoLangFunc)
				      topLevelOption).
				    getName().equals(parameters.stringLitNoLangFunc()))
				    throw new java.lang.Exception("Error: string-literal-without-language function " + 
								  ((OWL2TPTPParameters.StringLitNoLangFunc)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.stringLitNoLangFunc());
			    };
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.StringLitWithLangFunc) 
		    {
			if (parameters.stringLitWithLangFunc() == null)
			    {
				parameters.
				    setStringLitWithLangFunc(((OWL2TPTPParameters.StringLitWithLangFunc)
							    topLevelOption).
							   getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.StringLitWithLangFunc)
				      topLevelOption).
				    getName().equals(parameters.stringLitWithLangFunc()))
				    throw new java.lang.Exception("Error: string-literal-with-language function " + 
								  ((OWL2TPTPParameters.StringLitWithLangFunc)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.stringLitWithLangFunc());
			    };
		    }
		else if (topLevelOption instanceof OWL2TPTPParameters.TypedLitFunc) 
		    {
			if (parameters.typedLitFunc() == null)
			    {
				parameters.
				    setTypedLitFunc(((OWL2TPTPParameters.TypedLitFunc)
							    topLevelOption).
							   getName());
			    }
			else
			    {
				// Check if this is consistent with the previous value:
				if (!((OWL2TPTPParameters.TypedLitFunc)
				      topLevelOption).
				    getName().equals(parameters.typedLitFunc()))
				    throw new java.lang.Exception("Error: typed-literal function " + 
								  ((OWL2TPTPParameters.TypedLitFunc)
								   topLevelOption).
								  getName() +
								  " conflicts with the previous value " +
								  parameters.typedLitFunc());
			    };
		    }
		else
		    {
			assert false;
			return;
		    };
	    }; // for (Object topLevelOption : param.getTopLevelOption())

    } // parse(File file,Converter.Parameters parameter)

	
	
	
	
//  Data:


private Unmarshaller _unmarshaller;

} // class Parser