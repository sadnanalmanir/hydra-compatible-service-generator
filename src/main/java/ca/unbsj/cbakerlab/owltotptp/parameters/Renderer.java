package ca.unbsj.cbakerlab.owltotptp.parameters;

import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import ca.unbsj.cbakerlab.owltotptp.Converter;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.Functor;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.OWL2TPTPParameters;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.OtherReservedSymbols;
import ca.unbsj.cbakerlab.owltotptp.parameters.jaxb.URIToSymbols;

/** Renders owl2tptp.Converter parameters into XML. */
public class Renderer {


    /** Writes the XML representation of <code>parameters</code>
     *  into the stream.
     */
    public static void render(Converter.Parameters parameters,
		       OutputStream stream) 
	throws java.lang.Exception {
	
	OWL2TPTPParameters jaxbRepresentation = convert(parameters);

	JAXBContext jc = 
	    JAXBContext.newInstance("ca.unbsj.cbakerlab.owltotptp.parameters.jaxb");
	
	Marshaller marshaller = jc.createMarshaller();
	
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
			       Boolean.TRUE);
	
	marshaller.marshal(jaxbRepresentation,stream);


    } // render(Converter.Parameters parameters,..)


    
    
    private static OWL2TPTPParameters convert(Converter.Parameters parameters) {

	OWL2TPTPParameters result = new OWL2TPTPParameters();
	

	for (Map.Entry<String,Set<Converter.TPTPSymbolDescriptor>> entry :
		 parameters.URIsToTPTPSymbols().entrySet())
	    {
		URIToSymbols URISymbols = new URIToSymbols();
	
		URISymbols.setURI(entry.getKey());
		
		for (Converter.TPTPSymbolDescriptor desc : entry.getValue())
		    URISymbols.getFunctor().add(convert(desc));

		result.getTopLevelOption().add(URISymbols);

	    }; // for (Map.Entry<String,Set<Converter.TPTPSymbolDescriptor>> entry :



	OtherReservedSymbols reserved = new OtherReservedSymbols();
	    
	for (Converter.TPTPSymbolDescriptor desc : 
		 parameters.otherReservedSymbols())
	    reserved.getFunctor().add(convert(desc));
	    
	if (!reserved.getFunctor().isEmpty())
	    result.getTopLevelOption().add(reserved);



	if (parameters.dataDomainPredicate() != null)
	    {
		OWL2TPTPParameters.DataDomainPredicate dataDomainPred = 
		    new OWL2TPTPParameters.DataDomainPredicate();
	
		dataDomainPred.setName(parameters.dataDomainPredicate());

		result.getTopLevelOption().add(dataDomainPred);
	    };
	

	if (parameters.equalityPredicate() != null)
	    {
		OWL2TPTPParameters.EqualityPredicate equalityPred = 
		    new OWL2TPTPParameters.EqualityPredicate();
	
		equalityPred.setName(parameters.equalityPredicate());

		result.getTopLevelOption().add(equalityPred);
	    };
	
	if (parameters.intLitFunc() != null)
	    {
		OWL2TPTPParameters.IntLitFunc func = 
		    new OWL2TPTPParameters.IntLitFunc();
	
		func.setName(parameters.intLitFunc());

		result.getTopLevelOption().add(func);
	    };

	if (parameters.stringLitNoLangFunc() != null)
	    {
		OWL2TPTPParameters.StringLitNoLangFunc func = 
		    new OWL2TPTPParameters.StringLitNoLangFunc();
	
		func.setName(parameters.stringLitNoLangFunc());

		result.getTopLevelOption().add(func);
	    };
	
	if (parameters.stringLitWithLangFunc() != null)
	    {
		OWL2TPTPParameters.StringLitWithLangFunc func = 
		    new OWL2TPTPParameters.StringLitWithLangFunc();
	
		func.setName(parameters.stringLitWithLangFunc());

		result.getTopLevelOption().add(func);
	    };
	
	if (parameters.typedLitFunc() != null)
	    {
		OWL2TPTPParameters.TypedLitFunc func = 
		    new OWL2TPTPParameters.TypedLitFunc();
	
		func.setName(parameters.typedLitFunc());

		result.getTopLevelOption().add(func);
	    };
	

	return result;

    } // convert(Converter.Parameters parameters)


    private static Functor convert(Converter.TPTPSymbolDescriptor desc) {

	Functor result = new Functor();
	
	result.setPredicate(desc.isPredicate());
	result.setName(desc.name());
	result.setArity(new BigInteger("" + desc.arity())); // lexical cast :-(

	return result;

    } // convert(Converter.TPTPSymbolDescriptor desc)



} // class Renderer 