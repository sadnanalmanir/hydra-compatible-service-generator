package ca.unbsj.cbakerlab.owltotptp;

//import gnu.getopt.Getopt;
//import gnu.getopt.LongOpt;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedList;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import ca.unbsj.cbakerlab.owltotptp.parameters.Parser;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

//import org.semanticweb.owl.io.OWLXMLOntologyFormat;
//import org.semanticweb.owl.model.*;
public class OWL2TPTP {

    private static final Logger log = Logger.getLogger(OWL2TPTP.class);
    
    public static final String PARAMS_FILE_PATH = "/owltotptp/paramfiles/";
    public static final String INPUT_PARAM_FILE_NAME = "sample_parameters.xml";
    public static final String ACCUMULATED_PARAM_FILE_NAME = "new_parameters.xml";
    
    public static String translateOWLToTPTP(String ontologyURItoTPTP) {

        
        
        // contains the results after translation
        String convertedTPTPSyntax = "";

        boolean addCommonAxioms = true;
        boolean importClosure = true;
        boolean generateCNF = false;
        String ontologyURIStrings = null;
        
        String inputParamFilePath = "";
        String accumulatedParamFilePath = "";
        
        LinkedList<String> inputParameterFiles = new LinkedList<String>();
        String accumulatedParameterFile = null;

        // Getting param files from the resource folder
        try {
            inputParamFilePath = new File(log.getClass().getResource(PARAMS_FILE_PATH + INPUT_PARAM_FILE_NAME).toURI()).getPath();
            accumulatedParamFilePath = new File(log.getClass().getResource(PARAMS_FILE_PATH + ACCUMULATED_PARAM_FILE_NAME).toURI()).getPath();            
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        
        
        inputParameterFiles.add(inputParamFilePath);
        accumulatedParameterFile = accumulatedParamFilePath;
        
        
        
        ontologyURIStrings = ontologyURItoTPTP;

        Converter.Parameters parameters = new Converter.Parameters();

        try {

            // Parse the specified parameter files:
            Parser parametersParser
                    = new Parser();

            for (String parametersFileName : inputParameterFiles) {
                System.err.println("\n\n% Loading parameters from " + parametersFileName);

                File parametersFile = new File(parametersFileName);

                if (!parametersFile.isFile() || !parametersFile.canRead()) {
                    System.err.println("% Error: cannot read parameters from " + parametersFileName);
                    System.exit(1);
                };

                parametersParser.parse(parametersFile, parameters);

                System.err.println("% End of parameters file " + parametersFileName);

            }; // for (String parameterFileName : inputParameterFiles)
        } catch (Throwable ex) {
            System.err.println("% Error while reading parameters: " + ex);
            //ex.printStackTrace();
            System.exit(1);

        }; // catch (Throwable ex)

        //               Process the ontology:
        try {
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

            Renderer renderer = new Renderer(manager,
                    addCommonAxioms,
                    importClosure,
                    parameters,
                    generateCNF);

            System.err.println("\n\n% Processing ontology " + ontologyURIStrings);

            IRI ontologyURI = IRI.create(ontologyURIStrings);
            OWLOntology ontology = manager.loadOntology(ontologyURI);

            Writer stringBuffer = new StringWriter();
            renderer.render(ontology, stringBuffer);

            //Print the translated ontology
            //System.out.println(stringBuffer.toString());
            convertedTPTPSyntax = stringBuffer.toString();

            System.err.println("% End of ontology " + ontologyURIStrings);

        } catch (Exception ex) {
            System.out.println("Internal error: " + ex);
            ex.printStackTrace();
            System.exit(1);
        };

        // Dump the accumulated parameters:
        try {
            if (accumulatedParameterFile != null) {
                FileOutputStream stream
                        = new FileOutputStream(accumulatedParameterFile);

                ca.unbsj.cbakerlab.owltotptp.parameters.Renderer.render(parameters, stream);

                stream.close();

            }; // if (accumulatedParameterFile != null)

        } catch (FileNotFoundException ex) {
            System.err.println("% Error while writing accumulated parameters: " + ex);
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Unclassified error: " + ex);
            ex.printStackTrace();
            System.exit(1);
        };

        return convertedTPTPSyntax;
    }

    public static void main(String[] args) {
        boolean addCommonAxioms = false;
        boolean importClosure = false;
        boolean generateCNF = false;
        String[] ontologyURIStrings = null;
        LinkedList<String> inputParameterFiles = new LinkedList<String>();
        String accumulatedParameterFile = null;

        inputParameterFiles.add("sample_parameters.xml");
        accumulatedParameterFile = "new_parameters.xml";
        ontologyURIStrings = new String[1];
        ontologyURIStrings[0] = "http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl";

        Converter.Parameters parameters = new Converter.Parameters();

        try {

            //             Parse the specified parameter files:
            Parser parametersParser
                    = new Parser();

            for (String parametersFileName : inputParameterFiles) {
                System.err.println("\n\n% Loading parameters from " + parametersFileName);

                File parametersFile = new File(parametersFileName);

                if (!parametersFile.isFile() || !parametersFile.canRead()) {
                    System.err.println("% Error: cannot read parameters from " + parametersFileName);
                    System.exit(1);
                };

                parametersParser.parse(parametersFile, parameters);

                System.err.println("% End of parameters file " + parametersFileName);

            }; // for (String parameterFileName : inputParameterFiles)
        } catch (Throwable ex) {
            System.err.println("% Error while reading parameters: " + ex);
            //ex.printStackTrace();
            System.exit(1);

        }; // catch (Throwable ex)

        //               Process the ontology:
        try {
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

            Renderer renderer = new Renderer(manager,
                    addCommonAxioms,
                    importClosure,
                    parameters,
                    generateCNF);

            for (int i = 0; i < ontologyURIStrings.length; ++i) {
                System.err.println("\n\n% Processing ontology " + ontologyURIStrings[i]);

                /**
                 * ** enable this part to load from local file
                 *
                 * --cnf -c -p inputs/testsWithCourseOnt/sample_parameters.xml
                 * -a inputs/testsWithCourseOnt/new_parameters.xml -c
                 * inputs/testsWithCourseOnt/minimalHAI.owl
                 *
                 * File file = new File(ontologyURIStrings[i]); // load the
                 * local copy OWLOntology ontology =
                 * manager.loadOntologyFromOntologyDocument(file); // location
                 * the ontology was loaded from IRI ontologyURI =
                 * manager.getOntologyDocumentIRI(ontology); //****
                 */
                /**
                 * ********* enable this part to load from IRI *
                 */
                /*
                 * --cnf   -c -p inputs/testsWithCourseOnt/sample_parameters.xml -a inputs/testsWithCourseOnt/new_parameters.xml -c http://cbakerlab.unbsj.ca:8080/lubm/lubm-sadi-service-ontology.owl
                 */
                //URI ontologyURI = new URI(ontologyURIStrings[i]);
                IRI ontologyURI = IRI.create(ontologyURIStrings[i]);
                //OWLOntology ontology = manager.loadOntologyFromPhysicalURI(ontologyURI);
                OWLOntology ontology = manager.loadOntology(ontologyURI);

                Writer stringBuffer = new StringWriter();
                renderer.render(ontology, stringBuffer);

                System.out.println(stringBuffer.toString());

                System.err.println("% End of ontology " + ontologyURIStrings[i]);

            }; // for (int i = 0; i < ontologyURIStrings.length; ++i)

        } catch (Exception ex) {
            System.out.println("Internal error: " + ex);
            ex.printStackTrace();
            System.exit(1);
        };

        //               Dump the accumulated parameters:
        try {
            if (accumulatedParameterFile != null) {
                FileOutputStream stream
                        = new FileOutputStream(accumulatedParameterFile);

                ca.unbsj.cbakerlab.owltotptp.parameters.Renderer.render(parameters, stream);

                stream.close();

            }; // if (accumulatedParameterFile != null)

        } catch (FileNotFoundException ex) {
            System.err.println("% Error while writing accumulated parameters: " + ex);
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Unclassified error: " + ex);
            ex.printStackTrace();
            System.exit(1);
        };

    } // main(String[] args)

    private static void printUsage() {
        System.out.println("Usage: OWL2TPTP [OPTIONS] <ontology URI>+");
        System.out.println("Options:");
        System.out.println("\t--help -? \n\t\t Print this message.");
        System.out.println("\t--common_axioms -c \n\t\tInclude axioms that are common for all ontologies, e.g., that owl:Thing is nonempty.");
        System.out.println("\t--import_closure -i \n\t\tProcess the whole import closures of the ontologies.");
        System.out.println("\t--cnf -n \n\t\tClausify the results of conversion.");
        System.out.println("\t--parameters=file -p \n\t\tRead input parameters from the given XML file.");
        System.out.println("\t--accumulated_parameters=file -a \n\t\tSave the parameters in this XML file after all work is done, so that they can be used in later calls.");

    }
}
