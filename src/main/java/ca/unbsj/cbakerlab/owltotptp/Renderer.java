package ca.unbsj.cbakerlab.owltotptp;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Set;

import org.semanticweb.owlapi.io.AbstractOWLRenderer;
import org.semanticweb.owlapi.io.OWLRendererException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.util.OWLOntologyImportsClosureSetProvider;

import logic.is.power.tptp_parser.SimpleTptpParserOutput;
//import tptp_parser.SimpleTptpParserOutput;

public class Renderer extends AbstractOWLRenderer{

	  /** @param addCommonAxioms switches the output of axioms common for
     *         all ontologies, such as the axiom stating that 
     *         owl:Nothing is empty, etc.
     *  @param parameters various parameters that guide the conversion from
     *         the OWL abstract syntax to the TPTP abstract syntax; some of 
     *         the parameters are modified by the conversion within this object
     *         and can be used afterwards in other objects; eg, mappings
     *         of URIs to TPTP identifiers are augmented when new URIs are
     *         encountered.
     *  @param generateCNF indicates that the results must be in CNF
     */
    public Renderer(OWLOntologyManager ontologyManager,
		    boolean addCommonAxioms,
		    boolean importClosure,
		    Converter.Parameters parameters,
		    boolean generateCNF) { 
    	//deprecated 
    	//super(ontologyManager);
	_addCommonAxioms = addCommonAxioms;
	_importClosure = importClosure;
	_converter = new Converter(parameters,generateCNF);
    }

    /** Writes the given ontology onto the writer in the TPTP format. */
	@Override
    /** Writes the given ontology onto the writer in the TPTP format. */
    public void render(OWLOntology ontology,Writer writer) {
    
		
	    _printWriter = new PrintWriter(writer);
	    
	    if (_addCommonAxioms) 
		{
		    _printWriter.println("%  Common axioms:\n\n");

		    for (SimpleTptpParserOutput.TopLevelItem annForm :
			     _converter.commonAxioms())
			_printWriter.println(annForm.toString(2) + "\n\n");

		    _printWriter.println("%  End of common axioms.\n\n");
		};

	    if (_importClosure)
		{
		    // The set of all ontologies included in our ontology.
		    // Note that our ontology also belongs to the set.
		    
		    Set<OWLOntology> importClosure = 
			new OWLOntologyImportsClosureSetProvider(ontology.getOWLOntologyManager(),
								 ontology).getOntologies();
		    
		    for (OWLOntology ont : importClosure)
			for (SimpleTptpParserOutput.TopLevelItem annForm :
				 _converter.convert(ont))
			    _printWriter.println(annForm.toString(2) + "\n\n");
		}
	    else
		for (SimpleTptpParserOutput.TopLevelItem annForm :
			 _converter.convert(ontology))
		    _printWriter.println(annForm.toString(2) + "\n\n");
		
		

    } // renderOntology(OWLOntology ontology,..)

	   //                Data: 

    private boolean _addCommonAxioms;

    private boolean _importClosure;


    /** Where to print various objects. */
    private PrintWriter _printWriter;

    /** Common converter object for all ontologies to be processed. */
    private final Converter _converter;
 
}
