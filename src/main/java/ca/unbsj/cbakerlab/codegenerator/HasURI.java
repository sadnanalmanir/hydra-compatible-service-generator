/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ca.unbsj.cbakerlab.codegenerator;

public interface HasURI
{
	/**
	 * Returns the URI of the object.
	 * @return the URI of the object
	 */
	String getURI();
}