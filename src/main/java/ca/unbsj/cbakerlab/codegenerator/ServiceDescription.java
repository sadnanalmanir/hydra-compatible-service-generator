/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ca.unbsj.cbakerlab.codegenerator;

import java.util.Collection;

import ca.unbsj.cbakerlab.codegenerator.HasURI;

/**
 * An interface providing programmatic access to a SADI service description.
 * @author Luke McCarthy
 */
public interface ServiceDescription extends HasURI
{
	/**
	 * Returns the service URI.
	 * A SADI service is identified by a URI that is also an HTTP URL. 
	 * This URL is used to invoke the service as well as to identify it.
	 *  
	 * @return the URI of the service
	 */
	String getURI();

	/**
	 * Returns the service name, which may be null.
	 * A SADI service should have a short human-readable name.
	 * This is not required, but is encouraged.
	 * 
	 * @return the service name, which may be null
	 */
	String getName();


	/**
	 * Returns the service description, which may be null.
	 * A SADI service should have a detailed human-readable description.
	 * This is not required, but is encouraged.
	 * 
	 * @return the service description, which may be null
	 */
	String getDescription();


	/**
	 * Returns the service contact email address, which may be null.
	 * This is an email address that can be used to contact the service
	 * provider in the event that there are problems with the service.
	 * This is not required, but is encouraged and may one day be required.
	 * @return the service contact email address
	 */
	String getContactEmail();



	/**
	 * Returns the URI of the service's input OWL class.
	 * A SADI service has an input OWL class whose property restrictions
	 * describe the data that the service consumes. 
	 * This is required and the URI must resolve to a definition of the class.
	 * 
	 * @return the URI of the service's input OWL class
	 */
	String getInputClassURI();


	/**
	 * Returns the label of the service's input OWL class.
	 * @return the label of the service's input OWL class
	 */
	String getInputClassLabel();


	/**
	 * Returns the URI of the service's output OWL class.
	 * A SADI service has an output OWL class whose property restrictions
	 * describe the data that the service produces.
	 * This is required and the URI must resolve to a definition of the class.
	 * 
	 * @return the URI of the service's output OWL class
	 */
	String getOutputClassURI();

	/**
	 * Returns the label of the service's output OWL class.
	 * @return the label of the service's output OWL class
	 */
	String getOutputClassLabel();


	
}