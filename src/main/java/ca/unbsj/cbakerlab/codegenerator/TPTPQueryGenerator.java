/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.codegenerator;

import ca.unbsj.cbakerlab.sadi.params.Main;
import ca.unbsj.cbakerlab.sadi.params.ServiceGenerationException;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import org.apache.commons.lang.StringUtils;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

/**
 * Creates atoms and predicates to combine into a single specialized tptp query
 * Datatypes are not processed in input expression but in output expression the
 * datatypes are treated as target answer variable X wrapped inside ++answer(X).
 *
 * @author sadnana
 */
public class TPTPQueryGenerator {

    private static final Logger log = Logger.getLogger(TPTPQueryGenerator.class);

    ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
    Graph inputGraph = new TinkerGraph();
    Graph outputGraph = new TinkerGraph();

    Stack<Vertex> parentVerticesForInput = new Stack<Vertex>();
    static String propFromParentNodeForInput = "";

    Stack<Vertex> parentVerticesForOutput = new Stack<Vertex>();
    static String propFromParentNodeForOutput = "";

    String inputVarSymbol = "X";
    String outputVarSymbol = "Y";
    String ESCAPE_DOUBLE_QUOTE_SYMBOL = "\"";

    private String TPTP_SPECIALIZED_QUERY = "";
    private List<String> inAtomsPredicates = new LinkedList<String>();
    private List<String> outAtomsPredicates = new LinkedList<String>();
    //answer predicates are of the form ++answer(Y1, Y2, ..., YN)
    private List<String> answerVariables = new LinkedList<String>();
    private List<String> varsForSQLWhereClause = new LinkedList<String>();

    public String generateTPTPQuery(OWLClassExpression inputClassExpr, OWLClassExpression outputClassExpr) throws ServiceGenerationException {

        generateTreeFromInputExpr(inputClassExpr);
        generateTreeFromOutputExpr(outputClassExpr);
        displayInputGraphContent();
        displayOutputGraphContent();

        createInputAtomsPredicates();
        createOutputAtomsPredicates();
        assignSameRootNodeForBothGraphs();
        createAnswerAtomsPredicatesFromOutput(outputClassExpr);
        assignSameRootNodeForAnswerVars();
        String tptpQuery = creteTPTP_SPECIALIZED_QUERY();
        createVarsForSQLWhereClause(inputClassExpr);
        Map<String, String> nodeIdtoIndName = new HashMap<String, String>(getNodeIdtoIndName());
        tptpQuery = replaceInputIndNodesWithEntityPrefixMapping(tptpQuery, nodeIdtoIndName);
        tptpQuery = assignDoubleQuotesForInputVars(tptpQuery);

        return tptpQuery;
    }

    public void displayInputGraphContent() {
        log.info("----------------------------");
        log.info("Printing input graph content");
        log.info("----------------------------");
        log.info("[#]subject -- property --> object");
        for (Edge edge : inputGraph.getEdges()) {
            System.out.println(edge.getVertex(Direction.OUT).getId() + " # " + edge.getVertex(Direction.OUT).getProperty("name") + " --  " + edge.getLabel() + "  --> " + edge.getVertex(Direction.IN).getId() + " # " + edge.getVertex(Direction.IN).getProperty("name"));

        }
        log.info("--------Vertices--------");
        for (Vertex v : inputGraph.getVertices()) {
            System.out.println("Vertex:" + v.getId() + " -> " + v.toString() + " " + v.getProperty("name"));
        }
    }

    public void displayOutputGraphContent() {
        log.info("-----------------------------");
        log.info("Printing output graph content");
        log.info("-----------------------------");
        log.info("[#]subject -- property --> object");
        for (Edge edge : outputGraph.getEdges()) {
            System.out.println(edge.getVertex(Direction.OUT).getId() + " # " + edge.getVertex(Direction.OUT).getProperty("name") + " --  " + edge.getLabel() + "  --> " + edge.getVertex(Direction.IN).getId() + " # " + edge.getVertex(Direction.IN).getProperty("name"));
        }
        System.out.println("--------Vertices--------");
        for (Vertex v : outputGraph.getVertices()) {
            System.out.println("Vertex:" + v.getId() + " -> " + v.toString() + " " + v.getProperty("name"));
        }
    }

    public void createInputAtomsPredicates() {
        for (Edge edge : inputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                inAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                        + "(" + inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + ")");
            } else {
                inAtomsPredicates.add(edge.getLabel()
                        + "(" + inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + ", " + inputVarSymbol + edge.getVertex(Direction.IN).getId().toString() + ")");
            }
        }
    }

    public void createOutputAtomsPredicates() {
        for (Edge edge : outputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                outAtomsPredicates.add(edge.getVertex(Direction.IN).getProperty("name").toString()
                        + "(" + outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + ")");
            } else {
                outAtomsPredicates.add(edge.getLabel()
                        + "(" + outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString() + ", " + outputVarSymbol + edge.getVertex(Direction.IN).getId().toString() + ")");
            }
        }
    }

    public void assignSameRootNodeForBothGraphs() {
        //log.info("--------------------------------------------");
        log.info("Setting same root in output atoms/predciates");
        //log.info("--------------------------------------------");
        for (int i = 0; i < outAtomsPredicates.size(); i++) {
            String temp = outAtomsPredicates.get(i);
            if (temp.contains("Y0")) {
                outAtomsPredicates.set(i, temp.replace("Y0", "X0"));
            }
        }
    }

    public void createAnswerAtomsPredicatesFromOutput(OWLClassExpression classExpr) {

        //When there are datatypes , use them as answer variables
        for (Edge edge : outputGraph.getEdges()) {
            if (isDataproperty(edge.getLabel(), classExpr)) {
                answerVariables.add((outputVarSymbol + edge.getVertex(Direction.IN).getId().toString()));
            }
        }
        // if there are no answer variables acuired, there is no datatypes mentioned in the output expresion
        //  Consider individuals as answer variables e.g. type value Person
        if (answerVariables.isEmpty()) {
            for (Edge edge : outputGraph.getEdges()) {
                if (edge.getLabel().equals("type")) {
                    answerVariables.add((outputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()));
                }
            }
        }

    }

    private boolean isDataproperty(String property, OWLClassExpression classExpr) {
        /*Set<OWLEntity> entities = classExpr.getSignature();
         for (OWLEntity entity : entities) {
         if (entity.isOWLDataProperty()) {
         if (property.equals(entity.asOWLDataProperty().getIRI().getFragment())) {
         return true;
         }
         }
         }
         return false;
         */
        for (OWLDataProperty dataProp : classExpr.getDataPropertiesInSignature()) {
            if (property.equals(dataProp.getIRI().getFragment())) {
                return true;
            }
        }
        return false;
    }

    public void assignSameRootNodeForAnswerVars() {
        //log.info("-----------------------------------------------------");
        log.info("Setting same root for ++answer(X1, ... ,XN) Variables");
        //log.info("-----------------------------------------------------");
        for (int i = 0; i < answerVariables.size(); i++) {
            String temp = answerVariables.get(i);
            if (temp.contains("Y0")) {
                answerVariables.set(i, temp.replace("Y0", "X0"));
            }
        }
    }

    public String creteTPTP_SPECIALIZED_QUERY() {
        String comma = ",";
        for (int i = 0; i < inAtomsPredicates.size(); i++) {
            TPTP_SPECIALIZED_QUERY += "--" + "p_" + inAtomsPredicates.get(i);
            TPTP_SPECIALIZED_QUERY += ", ";
        }
        for (int i = 0; i < outAtomsPredicates.size(); i++) {
            TPTP_SPECIALIZED_QUERY += "--" + "p_" + outAtomsPredicates.get(i);
            TPTP_SPECIALIZED_QUERY += ", ";
        }
        TPTP_SPECIALIZED_QUERY += "++" + "answer" + "(";
        for (int i = 0; i < answerVariables.size(); i++) {
            TPTP_SPECIALIZED_QUERY += answerVariables.get(i);

            if (i < (answerVariables.size() - 1)) {
                TPTP_SPECIALIZED_QUERY += ", ";
            }
        }

        TPTP_SPECIALIZED_QUERY += ")";

        return TPTP_SPECIALIZED_QUERY;
    }

    public void createVarsForSQLWhereClause(OWLClassExpression classExpr) {

        // Find datatypes mentioned in the input class expression
        // 
        for (Edge edge : inputGraph.getEdges()) {
            if (isDataproperty(edge.getLabel(), classExpr)) {
                varsForSQLWhereClause.add((inputVarSymbol + edge.getVertex(Direction.IN).getId().toString()));
            }
        }
        // If there is no datatype property, check for instance of (OWLObjectHasValue) i.e. (type value Person)

        if (varsForSQLWhereClause.isEmpty()) {
            for (Edge edge : inputGraph.getEdges()) {
                if (edge.getLabel().equals("type")) {
                // This resource must be connected to ROOT node referring to "Resource input" form the processInput method
                    //if (edge.getLabel().equals("type") && edge.getVertex(Direction.OUT).getId().toString().equals("0")) {
                    varsForSQLWhereClause.add((inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()));
                }
            }
        }
        if (varsForSQLWhereClause.isEmpty()) {
            log.info("No value to instantiate in TPTP query. WHERE clause will have no input value to instantiate.");
        }

    }

    private Map<String, String> getNodeIdtoIndName() {
        Map<String, String> result = new HashMap<String, String>();
        for (Edge edge : inputGraph.getEdges()) {
            if (edge.getLabel().equals("type")) {
                String indNodeName = Main.PRIMARY_KEY_MAPPING_ATOM_PREFIX + edge.getVertex(Direction.IN).getProperty("name").toString();
                result.put((inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()), indNodeName + "(" + (inputVarSymbol + edge.getVertex(Direction.OUT).getId().toString()) + ")");
            }
        }
        return result;
    }

    private String replaceInputIndNodesWithEntityPrefixMapping(String tptpQuery, Map<String, String> nodeIdtoIndName) {
        for (Map.Entry<String, String> entry : nodeIdtoIndName.entrySet()) {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            if (tptpQuery.contains(entry.getKey())) {
                tptpQuery = tptpQuery.replaceAll(entry.getKey(), entry.getValue());
            }
        }
        return tptpQuery;
    }

    private String assignDoubleQuotesForInputVars(String tptpQuery) {
        for (String var : varsForSQLWhereClause) {
            if (tptpQuery.contains(var)) {
                tptpQuery = tptpQuery.replaceAll(var, ESCAPE_DOUBLE_QUOTE_SYMBOL + var + ESCAPE_DOUBLE_QUOTE_SYMBOL);
            }
        }
        return tptpQuery;
    }

    public Graph getInputExprGraph() {
        return inputGraph;
    }

    public Graph getOutputExprGraph() {
        return outputGraph;
    }

    public List<String> getVarsForSQLWhereClause() {
        return varsForSQLWhereClause;
    }

    public List<String> getAnswerVariables() {
        return answerVariables;
    }

    public void generateTreeFromInputExpr(OWLClassExpression owlClassExpr) throws ServiceGenerationException {

        if (owlClassExpr instanceof OWLObjectIntersectionOf) {

            Vertex v = inputGraph.addVertex(null);
            v.setProperty("name", r.render(owlClassExpr));

            if (propFromParentNodeForInput.equals("")) {
                // Create vertices equal to the number of operands as below and move on                
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            for (int i = 0; i < ((OWLObjectIntersectionOf) owlClassExpr).getOperands().size(); i++) {
                parentVerticesForInput.push(v);
            }
            for (OWLClassExpression y : ((OWLObjectIntersectionOf) owlClassExpr).getOperands()) {
                generateTreeFromInputExpr(y);
            }
        }

        if (owlClassExpr instanceof OWLObjectSomeValuesFrom) {

            String property = ((OWLObjectSomeValuesFrom) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection 
                // do nothing
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = ""; // this line may be ommitted                    
                    parentVerticesForInput.push(v1);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForInput = property;
            generateTreeFromInputExpr(((OWLObjectSomeValuesFrom) owlClassExpr).getFiller());
        }

        if (owlClassExpr instanceof OWLObjectExactCardinality) {

            String property = ((OWLObjectExactCardinality) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection 
                // do nothing
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = ""; // this line may be ommitted                    
                    parentVerticesForInput.push(v1);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForInput = property;
            generateTreeFromInputExpr(((OWLObjectExactCardinality) owlClassExpr).getFiller());
        }

        // shape: [V1:(type value ind)--Edge:type-> V2:ind]
        if (owlClassExpr instanceof OWLObjectHasValue) {

            String property = ((OWLObjectHasValue) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();
            OWLIndividual ind = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual();
            String indFragment = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual().getIRI().getFragment();

            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataSomeValuesFrom) {

            String property = ((OWLDataSomeValuesFrom) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataSomeValuesFrom) owlClassExpr).getFiller().asOWLDatatype();

            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataExactCardinality) {

            String property = ((OWLDataExactCardinality) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataExactCardinality) owlClassExpr).getFiller().asOWLDatatype();

            //Vertex v1 = inputGraph.addVertex(null);
            //v1.setProperty("name", r.render(owlClassExpr));
            if (propFromParentNodeForInput.equals("")) {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v2, property);
                } else {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = inputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForInput.isEmpty()) {
                    Vertex v1 = inputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = inputGraph.addEdge(null, parentVerticesForInput.pop(), v1, propFromParentNodeForInput);
                    propFromParentNodeForInput = "";

                    Vertex v2 = inputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = inputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }
        if (owlClassExpr instanceof OWLClass) {
            throw new ServiceGenerationException("Error: Unsupported OWLClass in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectUnionOf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectUnionOf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectComplementOf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectComplementOf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectAllValuesFrom) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectAllValuesFrom in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMinCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectMinCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMaxCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectMaxCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectHasSelf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectHasSelf in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectOneOf) {
            log.info("OWLObjectOneOf is supported in input class definition but IGNORED in creating atoms/predicates for creating TPTP query.");
        }
        if (owlClassExpr instanceof OWLDataAllValuesFrom) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataAllValuesFrom in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataHasValue) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataHasValue in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMinCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataMinCardinality in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMaxCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataMaxCardinality in input class expression: " + r.render(owlClassExpr));
        }
    }

    public void generateTreeFromOutputExpr(OWLClassExpression owlClassExpr) throws ServiceGenerationException {

        if (owlClassExpr instanceof OWLObjectIntersectionOf) {

            Vertex v = outputGraph.addVertex(null);
            v.setProperty("name", r.render(owlClassExpr));

            if (propFromParentNodeForOutput.equals("")) {
                // Create vertices equal to the number of operands as below and move on                
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            for (int i = 0; i < ((OWLObjectIntersectionOf) owlClassExpr).getOperands().size(); i++) {
                parentVerticesForOutput.push(v);
            }
            for (OWLClassExpression y : ((OWLObjectIntersectionOf) owlClassExpr).getOperands()) {
                generateTreeFromOutputExpr(y);
            }
        }

        if (owlClassExpr instanceof OWLObjectSomeValuesFrom) {

            String property = ((OWLObjectSomeValuesFrom) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection 
                // do nothing
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = ""; // this line may be ommitted                    
                    parentVerticesForOutput.push(v1);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForOutput = property;
            generateTreeFromOutputExpr(((OWLObjectSomeValuesFrom) owlClassExpr).getFiller());
        }

        if (owlClassExpr instanceof OWLObjectExactCardinality) {

            String property = ((OWLObjectExactCardinality) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                // (type value Student) and (has_advisor some (type value Advisor))
                // Use root from the stored vertex from intersection 
                // do nothing
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));
                    Edge e = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = ""; // this line may be ommitted                    
                    parentVerticesForOutput.push(v1);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }

            propFromParentNodeForOutput = property;
            generateTreeFromOutputExpr(((OWLObjectExactCardinality) owlClassExpr).getFiller());
        }

        // shape: [V1:(type value ind)--Edge:type-> V2:ind]
        if (owlClassExpr instanceof OWLObjectHasValue) {

            String property = ((OWLObjectHasValue) owlClassExpr).getProperty().asOWLObjectProperty().getIRI().getFragment();
            OWLIndividual ind = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual();
            String indFragment = ((OWLObjectHasValue) owlClassExpr).getValue().asOWLNamedIndividual().getIRI().getFragment();

            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(ind));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataSomeValuesFrom) {

            String property = ((OWLDataSomeValuesFrom) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataSomeValuesFrom) owlClassExpr).getFiller().asOWLDatatype();

            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }

        if (owlClassExpr instanceof OWLDataExactCardinality) {

            String property = ((OWLDataExactCardinality) owlClassExpr).getProperty().asOWLDataProperty().getIRI().getFragment();
            OWLDatatype datatype = ((OWLDataExactCardinality) owlClassExpr).getFiller().asOWLDatatype();

            //Vertex v1 = outputGraph.addVertex(null);
            //v1.setProperty("name", r.render(owlClassExpr));
            if (propFromParentNodeForOutput.equals("")) {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v2, property);
                } else {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e = outputGraph.addEdge(null, v1, v2, property);
                }
            } else {
                if (!parentVerticesForOutput.isEmpty()) {
                    Vertex v1 = outputGraph.addVertex(null);
                    v1.setProperty("name", r.render(owlClassExpr));

                    Edge e1 = outputGraph.addEdge(null, parentVerticesForOutput.pop(), v1, propFromParentNodeForOutput);
                    propFromParentNodeForOutput = "";

                    Vertex v2 = outputGraph.addVertex(null);
                    v2.setProperty("name", r.render(datatype));
                    Edge e2 = outputGraph.addEdge(null, v1, v2, property);
                } else {
                    throw new ServiceGenerationException("TPTP Query Creation Error: Parent node not found for " + r.render(owlClassExpr));
                }
            }
        }
        if (owlClassExpr instanceof OWLClass) {
            throw new ServiceGenerationException("Error: Unsupported OWLClass in input class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectUnionOf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectUnionOf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectComplementOf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectComplementOf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectAllValuesFrom) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectAllValuesFrom in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMinCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectMinCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectMaxCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectMaxCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectHasSelf) {
            throw new ServiceGenerationException("Error: Unsupported OWLObjectHasSelf in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLObjectOneOf) {
            log.info("OWLObjectOneOf is supported in input class definition but IGNORED in creating atoms/predicates for creating TPTP query.");
        }
        if (owlClassExpr instanceof OWLDataAllValuesFrom) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataAllValuesFrom in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataHasValue) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataHasValue in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMinCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataMinCardinality in output class expression: " + r.render(owlClassExpr));
        }
        if (owlClassExpr instanceof OWLDataMaxCardinality) {
            throw new ServiceGenerationException("Error: Unsupported OWLDataMaxCardinality in output class expression: " + r.render(owlClassExpr));
        }
    }

}
