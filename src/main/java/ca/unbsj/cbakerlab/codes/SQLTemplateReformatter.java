/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.codes;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author sadnana
 */
public class SQLTemplateReformatter {

    private static final Logger log = Logger.getLogger(SQLTemplateReformatter.class);

    public String reformatSQLTemplateQuery(String templateSQLQuery) {

        String reformattedSQLQuery = "";
        String line = "";

        // if there is WHERE SOMETHING = "DATA_VALUE"
        if (templateSQLQuery.contains("WHERE") && templateSQLQuery.contains("\"")) {
            Set<String> paramVars = new HashSet<String>();
            String[] arr = StringUtils.substringsBetween(templateSQLQuery, "\"", "\"");
            // no duplicate parameter variables, 
            for (String param : arr) {
                paramVars.add(param);
            }
            for (String pVar : paramVars) {
                templateSQLQuery = StringUtils.replace(templateSQLQuery, pVar, "+" + pVar + "+");
            }
        } else {
            log.info("Data value instantiation in WHERE clause is missing.");
        }

        Scanner scanner = new Scanner(templateSQLQuery);
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            //if not the last line
            if (scanner.hasNextLine()) {
                if (line.length() > 0) {
                    reformattedSQLQuery += "\n\t\t\t\"" + " " + line + "\"+";
                }
            } else //last line not requiring +
            {
                reformattedSQLQuery += "\n\t\t\t\"" + " " + line + "\"";
            }
        }
        scanner.close();

        if (StringUtils.contains(reformattedSQLQuery, "+\"\"+")) {
            reformattedSQLQuery = StringUtils.replace(reformattedSQLQuery, "+\"\"+", "+");
        }

        return reformattedSQLQuery;
    }

}
