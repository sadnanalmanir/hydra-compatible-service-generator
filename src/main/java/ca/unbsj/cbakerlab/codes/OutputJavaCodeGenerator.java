/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ca.unbsj.cbakerlab.codes;

import static ca.unbsj.cbakerlab.sqltemplate.querymanager.sail.query_manager.SQLGenerator.tableColumns;
import com.tinkerpop.blueprints.Graph;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClassExpression;
/**
 *
 * @author sadnana
 */
public class OutputJavaCodeGenerator {
    private static final Logger log = Logger.getLogger(OutputJavaCodeGenerator.class);
    // serialized table columns according to the output class descriptions ordering
    List<String> rdbTableColNames = new ArrayList<String>(tableColumns);
    
    
    public void generateOutputJavaCode(OWLClassExpression outputClassExpr, Graph outputGraph, List<String> answerVariables) {
        log.info("table columns: "+rdbTableColNames.toString());
    }

    public String getAUTO_GENERATED_OUTPUT_CODE() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
