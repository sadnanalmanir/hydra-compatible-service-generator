package ca.unbsj.cbakerlab.sqltemplate;

import static ca.unbsj.cbakerlab.sadi.params.Main.JAVA_IO_TEMP_DIR_NAME;
import static ca.unbsj.cbakerlab.sadi.params.Main.PREDICATE_VIEWS_XML_FILENAME;
import static ca.unbsj.cbakerlab.sadi.params.Main.SQL_TEMPLATE_DIR_NAME;
import ca.unbsj.cbakerlab.sqltemplate.querymanager.SAILQueryManager;
import ca.unbsj.cbakerlab.sqltemplate.schematicanswers.SchematicAnswersGenerator;

/**
 * Created by sadnana on 03/07/15.
 */
public class TemplateSQLQueryGenerator {

    public static void main(String[] args) {

        String tptpQuery
                = "include('rdb2ont.tptp').\n"
                + "\n"
                + "% HAI.owl ontology translated into tptp formulas without illegal tptp formula symbols \n"
                + "include('HAI_no_Illegal_Symbols.ontology.fof.tptp').\n"
                + "\n"
                + "% semantic query\n"
                + "input_clause(query4patOacisPID,conjecture,\n"
                + "  [\n"
                //+ "   --p_has_facility_description(Y, X),\n"
                //+ "   --p_Patient(X),\n"
                + "   --p_Diagnosis(X),\n"
                + "    ++answer(X)\n"
                + "  ]).";
        
        tptpQuery
                = "include('rdb2ont.tptp').\n"
                + "\n"
                + "% HAI.owl ontology translated into tptp formulas without illegal tptp formula symbols \n"
                + "include('HAI_no_Illegal_Symbols.ontology.fof.tptp').\n"
                + "\n"
                + "% semantic query\n"
                + "input_clause(query4patOacisPID,conjecture,\n"
                + "  [\n"
                + "   --p_has_facility_description(X0, Y1),\n"
                + "   --p_has_first_name(Z, Y2),\n"
                //+ "   --p_Patient(X),\n"
                //+ "   --p_Diagnosis(X),\n"                
                + "    ++answer(Y1, Y2)\n"
                + "  ]).";

        SchematicAnswersGenerator schematicAnswersGenerator = new SchematicAnswersGenerator(tptpQuery);
        String schematicAnswers = schematicAnswersGenerator.generateSchematicAnswers();

        System.out.println(schematicAnswers);
        
         SAILQueryManager sailQueryManager = new SAILQueryManager();
         args = new String[]{"--output-xml", "off", "--ecnomical-joins", "off", "--table-query-patterns", System.getProperty(JAVA_IO_TEMP_DIR_NAME).concat("/" + SQL_TEMPLATE_DIR_NAME) + "/" + PREDICATE_VIEWS_XML_FILENAME};
         try {
         // the args are empty here, the callee method has the assigned arguments
         sailQueryManager.generateSQLTemplate(args, schematicAnswers);
         } catch (Exception e) {
         e.printStackTrace();
         }
         
    }

    public static void generateSQLfromVP(String[] args) {

        String tptpQuery
                = "include('tohdw_haio_semantic_map.fof.tptp').\n"
                + "\n"
                + "% HAI.owl ontology translated into tptp formulas without illegal tptp formula symbols \n"
                + "include('HAI_no_Illegal_Symbols.ontology.cnf.tptp').\n"
                + "\n" + "% semantic query" + "\n"
                + "input_clause(query4patOacisPID,conjecture,\n" + "  [\n"
                + "   --p_Patient(X),\n"
                + "   --p_has_facility_description(X,N),\n"
                + "    ++answer(N)\n" + "  ]).";

        tptpQuery
                = "include('rdb2ont.tptp').\n"
                + "\n"
                + "% HAI.owl ontology translated into tptp formulas without illegal tptp formula symbols \n"
                + "include('HAI_no_Illegal_Symbols.ontology.cnf.tptp').\n"
                + "\n" + "% semantic query" + "\n"
                + "input_clause(query4patOacisPID,conjecture,\n" + "  [\n"
                + "   --p_Patient(X),\n"
                + "   --p_has_facility_description(X,N),\n"
                + "    ++answer(N)\n" + "  ]).";

        SchematicAnswersGenerator schematicAnswersGenerator = new SchematicAnswersGenerator(tptpQuery);
        String schematicAnswers = schematicAnswersGenerator.generateSchematicAnswers();

        System.out.println(schematicAnswers);
        SAILQueryManager sailQueryManager = new SAILQueryManager();
        try {
            // the args are empty here, the callee method has the assigned arguments
            sailQueryManager.generateSQLTemplate(args, schematicAnswers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
