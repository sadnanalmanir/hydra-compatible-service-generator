package ca.unbsj.cbakerlab.sadi.params;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.conn.HttpHostConnectException;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URISyntaxException;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxClassExpressionParser;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Logger;




public class ServiceParamExtractor {

    private static final Logger log = Logger.getLogger(ServiceParamExtractor.class);
    private static final String DESCRIPTION_VALIDATOR_FILE_NAME = "SADI_service_description_validator.jar";
    private String serviceOntologyURI;  
    private String inputClassURI;
    private String outputClassURI;  

    public ServiceParamExtractor(String serviceOntologyURI) {
        this.serviceOntologyURI = serviceOntologyURI;
    }

    
    

    public void init(boolean runDocumentResetter,
            boolean runTokenizer,
            boolean runSentenceSplitter,
            boolean runPosTagger,
            boolean runNpChunker,
            boolean runChemicalExtractor,
            boolean runDiseaseExtractor,
            boolean runCooccurrenceExtractor
    )  {

        
    }
    
    
    public boolean isValidURI(String targetUrl) {
            
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");

            // Set timeouts in milliseconds
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setReadTimeout(30000);

            // Print HTTP status code/message for your information.
            //System.out.println("Response Code: " + httpUrlConn.getResponseCode());
            //System.out.println("Response Message: " + httpUrlConn.getResponseMessage());

            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }
    
    public String getInputURIFromServiceOntology(String serviceOntologyURI){
    
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      OWLDataFactory df = OWLManager.getOWLDataFactory();
      IRI ontologyIRI = IRI.create(serviceOntologyURI);
      
      try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for(OWLClass cls : classes) {
              if(cls.getIRI().getFragment().contains("Input") || cls.getIRI().getFragment().contains("input")) {
            	this.inputClassURI = cls.getIRI().toString();
              }              
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }  
       return inputClassURI;   
    }
    
    public String getOutputURIFromServiceOntology(String serviceOntologyURI){
    
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      OWLDataFactory df = OWLManager.getOWLDataFactory();
      IRI ontologyIRI = IRI.create(serviceOntologyURI);
      
      try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for(OWLClass cls : classes) {
              if(cls.getIRI().getFragment().contains("Output") || cls.getIRI().getFragment().contains("output")) {
            	this.outputClassURI = cls.getIRI().toString();
              }
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }    
      return outputClassURI;  
    }
    
    
    public boolean isInputHYDRACompatible(String inputClassURI) {
    
    	String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(log.getClass().getResource("/" + DESCRIPTION_VALIDATOR_FILE_NAME).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true --input " + inputClassURI;
        String inputValidationResult = executeCommand(command);
        log.info("HYDRA Service Input validator check: " + inputValidationResult);
        
        if (!(      
                    StringUtils.contains(inputValidationResult, "Undefined nonterminal in input class")
                 || StringUtils.contains(inputValidationResult, "Cannot retrieve the definition for input class")
                 )
               )
               return true;
        
        return false;	    
    }
    
    
    public boolean isOutputHYDRACompatible(String outputClassURI) {
    
    	String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(log.getClass().getResource("/" + DESCRIPTION_VALIDATOR_FILE_NAME).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true" + " --output " + outputClassURI;
        String outputValidationResult = executeCommand(command);
        log.info("HYDRA Service Input validator check: " + outputValidationResult);
        
        if (!(      
    			  StringUtils.contains(outputValidationResult, "Undefined nonterminal in output class")
                 || StringUtils.contains(outputValidationResult, "Cannot retrieve the definition for output class") 
                 )
               )
               return true;
        
        return false;	    
    }
    
    private static String executeCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader =
                           new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}
    
    
    public String getInputClassExpr(String serviceOntologyURI, String inputClassURI){
    
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      OWLDataFactory df = OWLManager.getOWLDataFactory();
      IRI ontologyIRI = IRI.create(serviceOntologyURI);
      ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
      ManchesterOWLSyntaxClassExpressionParser parser = new ManchesterOWLSyntaxClassExpressionParser(df, null);
      String owlInputClassExpr = "";
      try {            
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            OWLClass inputClass = df.getOWLClass(IRI.create(inputClassURI));
            OWLClassExpression inputClassExpr = getClassExpr(ontology, inputClass);
            owlInputClassExpr = inputClassExpr.toString();
            log.info(owlInputClassExpr);
            // Get OWL class expr as manchester expr string
            String inputManchesterClassExpr = getClassExprInManchesterSyntax(ontology, inputClass, r);
            log.info(inputManchesterClassExpr); 
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }  
       return owlInputClassExpr;   
    }
    
    public String getOutputClassExpr(String serviceOntologyURI, String outputClassURI){
    
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      OWLDataFactory df = OWLManager.getOWLDataFactory();
      IRI ontologyIRI = IRI.create(serviceOntologyURI);
      String owlOutputClassExpr = "";
      try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            OWLClass outputClass = df.getOWLClass(IRI.create(outputClassURI));
            OWLClassExpression outputClassExpr = getClassExpr(ontology, outputClass);
            owlOutputClassExpr = outputClassExpr.toString();
            log.info(owlOutputClassExpr);
            
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }  
       return owlOutputClassExpr;   
    }


    private OWLClassExpression getClassExpr(OWLOntology ontology, OWLClass owlClass) {
        OWLClassExpression owlClsExpr = null;

        if (isDefinedAsEquivalent(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getEquivalentClasses(ontology)) {
                owlClsExpr = eca;
            }
        } else if (isDefinedAsSubclass(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getSuperClasses(ontology)) {
                owlClsExpr = eca;
            }
        }
        return owlClsExpr;
    }
    
    private boolean isDefinedAsEquivalent(OWLOntology ontology, OWLClass owlClass) {
        if (owlClass.getEquivalentClasses(ontology).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isDefinedAsSubclass(OWLOntology ontology, OWLClass owlClass) {
        if (owlClass.getSuperClasses(ontology).size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    private String getClassExprInManchesterSyntax(OWLOntology ontology, OWLClass cls, ManchesterOWLSyntaxOWLObjectRendererImpl r) {

        String manchesterOWLClsExpr = null;
        if (isDefinedAsEquivalent(ontology, cls)) {
            for (OWLClassExpression eca : cls.getEquivalentClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        } else if (isDefinedAsSubclass(ontology, cls)) {
            for (OWLClassExpression eca : cls.getSuperClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        }
        return manchesterOWLClsExpr;
    }
    
    public List<String> getImportedOntologies(String serviceOntologyURI) {

    	List<String> domainOntologies = new ArrayList<String>();
    	    
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      OWLDataFactory df = OWLManager.getOWLDataFactory();
      IRI ontologyIRI = IRI.create(serviceOntologyURI);      
      try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            Set<OWLImportsDeclaration> importedOntologies = ontology.getImportsDeclarations();
            for(OWLImportsDeclaration importedOnt : importedOntologies) {
            	domainOntologies.add(importedOnt.getIRI().getFragment());
            	log.info(importedOnt.getIRI().getFragment());
            }
            
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }  
    	
    
    
      return domainOntologies;
    }

    /**
     * Tell MutationGrounderApp's controller about the corpus you want to run on
     */
    public void setCorpus() {
        //annieController.setCorpus(corpus);
    }

    /**
     * Run MutationGrounderApp
     */
    public void execute(){
        log.info("Running Pipeline...");
        

        log.info("...Pipeline complete");
    }

}
