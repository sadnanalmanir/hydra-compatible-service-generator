package ca.unbsj.cbakerlab.sadi.params;

import ca.unbsj.cbakerlab.codegenerator.ServiceCodeGenerator;
import ca.unbsj.cbakerlab.codegenerator.TPTPQueryGenerator;
import ca.unbsj.cbakerlab.codes.InputJavaCodeGenerator;
import ca.unbsj.cbakerlab.codes.OutputJavaCodeGenerator;
import ca.unbsj.cbakerlab.codes.SQLTemplateReformatter;
import ca.unbsj.cbakerlab.owltotptp.OWL2TPTP;
import ca.unbsj.cbakerlab.sqltemplate.querymanager.SAILQueryManager;
import ca.unbsj.cbakerlab.sqltemplate.schematicanswers.SchematicAnswersGenerator;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.tinkerpop.blueprints.Graph;
import java.util.LinkedList;
import org.semanticweb.owlapi.model.OWLClassExpression;

public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    // Service I/O Validity checker library
    public static final String DESCRIPTION_VALIDATOR_FILE_NAME = "SADI_service_description_validator.jar";

    // OWL2TPTP specifications
    public static final String DOMAIN_ONTOLOGY_TPTP_SYNTAX_PATH = "domain-ontology/foftptp/";

    // SQL Template Query generation specifications
    // Change these two files
    public static final String ONT_RDB_MAPPING_FILENAME = "rdb2ont.tptp";
    //public static final String ONTOLOGY_IN_TPTP_FILENAME = "HAI_no_Illegal_Symbols.ontology.fof.tptp";
    public static final String ONTOLOGY_IN_TPTP_FILENAME = "HAI.owl.tptp";
    public static final String PRIMARY_KEY_MAPPING_ATOM_PREFIX = "entityFor";

    public static final String SCHEMATIC_ANSWERS_FILE_NAME = "schematic_answers.xml";
    public static final String SQL_TEMPLATE_DIR_NAME = "SQLTemplateDir";
    public static final String JAVA_IO_TEMP_DIR_NAME = "java.io.tmpdir";
    public static final String EXTENSIONAL_PREDICATES_FILE_NAME = "extensional_predicates.xml";
    public static final String ANSWER_PREDICATES_FILE_NAME = "answer_predicates.xml";
    public static final String PREDICATE_VIEWS_XML_FILENAME = "predicate_views.xml";
    public static final String PREDICATE_VIEWS_SCHEMA_FILENAME = "PredicateViews.xsd";

    // Service code generation specifications    
    public static final String AUTO_GENERATED_SERVICE_DIR = "autogen-sadiwebservice";
    public static final String SERVICE_PACKAGE_STRUCTURE = "ca.unbsj.cbakerlab.sadi.services";
    public static final String SOURCE_DIRECTORY = "src/main/java";
    public static final String RESOURCES_DIRECTORY = "src/main/resources";
    public static final boolean IS_SYNCHRONOUS_SERVICE = false;

    public static final String DB_CLASS_NAME = "MySqlDatabase";
    public static final String VOCAB_FILE_NAME = "Vocab";
    public static final String README_FILENAME = "README";
    public static final String WEB_XML_PATH = "src/main/webapp/WEB-INF/web.xml";
    public static final String INDEX_JSP_PATH = "src/main/webapp/index.jsp";
    public static final String DB_PROPERTY_FILENAME = "database";

    // Prefix and suffix for the URIs
    public static final String URI_PREFIX = "http://cbakerlab.unbsj.ca:8080/dw/model/function/";
    public static final String URI_SUFFIX = "/val?val";
    
    private final String ESCAPE_DOUBLE_QUOTE_SYMBOL = "\"";

    private static String createTptpQueryAtomsPredicates(String ONTOLOGY_IN_TPTP_FILENAME, String ONT_RDB_MAPPING_FILENAME, String tptpQueryAtomsPredicates) {
        String TPTP_QUERY = 
                
                  "include('" + ONT_RDB_MAPPING_FILENAME + "').\n" 
                + "include('" + ONTOLOGY_IN_TPTP_FILENAME + "').\n"
                + "\n"                
                + "input_clause(query,conjecture,\n"
                + "[\n"
                + tptpQueryAtomsPredicates
                + "]).";
        
        
        
        return TPTP_QUERY;
    }

    public Main() {

    }

    /**
     * Executed from main. Supports JSAP parser to read arguments from command
     * line. Details of how to use arguments in Main please read in readme
     *
     * @param args arguments from command line
     * @throws Exception
     */
    public static void run(String args) {

        try {
            long time = System.currentTimeMillis();

            // Get command line arguments.
            String serviceOntologyURI = null;
            String serviceName = null;
            String inputClassURI = null;
            String outputClassURI = null;
            String email = null;
            String description = null;

            boolean needSeedInputData = false;
            boolean runGenerator = false;

            //
            // Define options.
            //
            {
                JSAP jsap = new JSAP();
                {
                    FlaggedOption s = new FlaggedOption("serviceOntologyURI").setStringParser(JSAP.STRING_PARSER).setLongFlag("Service Ontology URI").setRequired(true).setShortFlag('s');
                    s.setHelp("URI of the service ontology.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("email").setStringParser(JSAP.STRING_PARSER).setLongFlag("email").setRequired(true).setShortFlag('e');
                    s.setHelp("The contact email of the author.");
                    jsap.registerParameter(s);
                }

                {
                    FlaggedOption s = new FlaggedOption("description").setStringParser(JSAP.STRING_PARSER).setLongFlag("description").setRequired(true).setShortFlag('d');
                    s.setHelp("The description of the sevice.");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("needSeedInputData").setLongFlag("seed");
                    s.setHelp("needSeedInputData: Service allPatients from a table do NOT need input values.");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("runGenerator").setLongFlag("generate");
                    s.setHelp("runGenerator");
                    jsap.registerParameter(s);
                }

                // Parse the command-line arguments.
                JSAPResult config = jsap.parse(args);

                // Display the helpful help in case trouble happens.
                if (!config.success()) {
                    log.info("=============================");
                    log.info("* Problem with command line *");
                    log.info("=============================");
                    System.err.println();
                    System.err.println(" " + jsap.getUsage());
                    System.err.println();
                    System.err.println(jsap.getHelp());

                    Iterator<?> messageIterator = config.getErrorMessageIterator();
                    while (messageIterator.hasNext()) {
                        System.err.println(messageIterator.next());
                    }
                    System.exit(1);
                }

                // Assign to variables.
                log.info("=======================");
                log.info("* Parameters assigned *");
                log.info("=======================");
                serviceOntologyURI = config.getString("serviceOntologyURI");
                if (serviceOntologyURI != null) {
                    log.info("Service Ontology URI: " + serviceOntologyURI);
                }

                email = config.getString("email");
                if (email != null) {
                    log.info("Contact email: " + email);
                }

                description = config.getString("description");
                if (description != null) {
                    log.info("Description: [Currently no space allowed, Use underscore instead.] " + description);
                }

                needSeedInputData = config.getBoolean("needSeedInputData");
                if (needSeedInputData) {
                    log.info("needSeedInputData [e.g. getXByY]: " + needSeedInputData);
                }
                // Modules.
                //
                runGenerator = config.getBoolean("runGenerator");
                if (runGenerator) {
                    log.info("runGenerator: " + runGenerator);
                }
            }

            //
            // RUN Generator.
            //	
            if (runGenerator) {

                ServiceParamExtractor extractor = new ServiceParamExtractor(serviceOntologyURI);

                serviceName = extractor.getServiceNameFromURI(serviceOntologyURI);

                if (!extractor.isValidURI(serviceOntologyURI)) {
                    //log.info("Invalid Service ontology :" + serviceOntologyURI);
                    throw new ServiceGenerationException("Invalid Service ontology :" + serviceOntologyURI);
                }
                inputClassURI = extractor.getInputURIFromServiceOntology(serviceOntologyURI);
                if (!extractor.isValidURI(inputClassURI)) {
                    //log.info("Invalid Service Input Class URI :" + inputClassURI);
                    throw new ServiceGenerationException("Invalid Service Input Class URI :" + inputClassURI);
                }
                outputClassURI = extractor.getOutputURIFromServiceOntology(serviceOntologyURI);
                if (!extractor.isValidURI(outputClassURI)) {
                    //log.info("Invalid Service Output Class URI :" + outputClassURI);
                    throw new ServiceGenerationException("Invalid Service Output Class URI :" + outputClassURI);
                }
                log.info("================================================");
                log.info("* Checking strict compatibility of Input class *");
                log.info("================================================");
                if (!extractor.isInputHYDRACompatible(inputClassURI, DESCRIPTION_VALIDATOR_FILE_NAME)) {
                    //log.info("Input incompatible with HYDRA:" + inputClassURI);		  	
                    throw new ServiceGenerationException("Input incompatible with HYDRA:" + inputClassURI);
                }
                log.info("=================================================");
                log.info("* Checking strict compatibility of Output class *");
                log.info("=================================================");

                if (!extractor.isOutputHYDRACompatible(outputClassURI, DESCRIPTION_VALIDATOR_FILE_NAME)) {
                    //log.info("Output incompatible with HYDRA:" + outputClassURI);
                    throw new ServiceGenerationException("Output incompatible with HYDRA:" + outputClassURI);
                }
                log.info("==================================");
                log.info("* Getting Input class expression *");
                log.info("==================================");
                OWLClassExpression inputClassExpr = extractor.getInputClassExpr(serviceOntologyURI, inputClassURI);
                log.info(inputClassExpr);
                log.info("===================================");
                log.info("* Getting Output class expression *");
                log.info("===================================");

                OWLClassExpression outputClassExpr = extractor.getOutputClassExpr(serviceOntologyURI, outputClassURI);
                log.info(outputClassExpr);
                log.info("=======================================================");
                log.info("* Getting ontologies imported in the Service ontology *");
                log.info("=======================================================");

                List<String> domainOntologyURIS = extractor.getImportedOntologies(serviceOntologyURI);

                //String[] vals = {"--cnf",   "-c", "-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
                //String[] vals = {"-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
                //OWL2TPTP.convert(vals);
                log.info("====================================================================");
                log.info("* Translate and save imported domain ontologies into TPTP formulas *");
                log.info("====================================================================");

                /**
                 * Translating using ontologies to tptp jar file. Currently, we
                 * don't need the tptp-parser.jar as programmable conversion is
                 * possible with the tptp-parser source code.
                 *
                 * for (String domainOntologyURI : domainOntologyURIS) { if
                 * (!extractor.isValidURI(domainOntologyURI)) {
                 * //log.info("Invalid Domain ontology :" + domainOntology);
                 * throw new ServiceGenerationException("Invalid Domain ontology
                 * :" + domainOntologyURI); } else { String domainOntologyName =
                 * extractor.getNameOfOntology(domainOntologyURI);
                 * log.info("Translation begins for : " + domainOntologyName);
                 * String ontologyFofInTPTP =
                 * extractor.translateOWLToTPTP(domainOntologyURI);
                 * log.info("Translation ends."); log.info("\n"); String
                 * pathSaved =
                 * extractor.storeOntologyFofInTPTP(domainOntologyName,
                 * ontologyFofInTPTP); log.info("Saved in : " + pathSaved); } }
                 */
                /**
                 * Programmable way to converting OWL to TPTP format .
                 */
                for (String domainOntologyURI : domainOntologyURIS) {
                    if (!extractor.isValidURI(domainOntologyURI)) {
                        //log.info("Invalid Domain ontology :" + domainOntology);
                        throw new ServiceGenerationException("Invalid Domain ontology :" + domainOntologyURI);
                    } else {
                        String domainOntologyName = extractor.getNameOfOntology(domainOntologyURI);
                        log.info("Translation begins for : " + domainOntologyName);
                        String ontologyFofInTPTP = OWL2TPTP.translateOWLToTPTP(domainOntologyURI);
                        // Enable to see the translation in log
                        //log.info("----- Beginning of " + domainOntologyName + " in TPTP FOF syntax -----");
                        //log.info(ontologyFofInTPTP);
                        //log.info("----- End of " + domainOntologyName + " in TPTP FOF syntax -----");                        
                        log.info("Translation ends.");
                        String pathSaved = extractor.storeOntologyFofInTPTP(DOMAIN_ONTOLOGY_TPTP_SYNTAX_PATH, domainOntologyName, ontologyFofInTPTP);
                        log.info("Translated Ontology Saved as : " + pathSaved);
                    }
                }

                log.info("====================================================");
                log.info("* Checking if a tptp query is possible to generate *");
                log.info("====================================================");
                // There must be OWLObjectHasValue or OWLDatatypeProperty present in the expression
                if (outputClassExpr.getDataPropertiesInSignature().isEmpty() && outputClassExpr.getObjectPropertiesInSignature().isEmpty()) {
                    throw new ServiceGenerationException("Error: Cannot create TPTP Query. No individuals and datatypes found in output class expression.");
                }
                log.info("====================================================");
                log.info("* Generating TPTP Query from I/O Class Expressions *");
                log.info("====================================================");
                TPTPQueryGenerator tptpQueryGenerator = new TPTPQueryGenerator();
                String tptpQueryAtomsPredicates = tptpQueryGenerator.generateTPTPQuery(inputClassExpr, outputClassExpr);
                log.info("-----------------------------------------------------------");
                log.info("* Printing TPTP Query generated from I/O Class Expression *");
                log.info("-----------------------------------------------------------");
                log.info(tptpQueryAtomsPredicates);
                String tptpQuery = createTptpQueryAtomsPredicates(ONTOLOGY_IN_TPTP_FILENAME, ONT_RDB_MAPPING_FILENAME, tptpQueryAtomsPredicates);
                log.info("TPTP QUERY :\n" +tptpQuery);
                // getting the processed graphs and variables
                Graph inputGraph = tptpQueryGenerator.getInputExprGraph();
                Graph outputGraph = tptpQueryGenerator.getOutputExprGraph();
                List<String> varsForSQLWhereClause = new LinkedList<String>(tptpQueryGenerator.getVarsForSQLWhereClause());
                List<String> answerVariables = new LinkedList<String>(tptpQueryGenerator.getAnswerVariables());
//tptpQueryFromIOClassExpr
                log.info("=================================");
                log.info("* Generating SQL Query Template *");
                log.info("=================================");
                String autoGeneratedSQLQuery = generateSQLQueryTemplate(tptpQuery).trim();
                if (autoGeneratedSQLQuery.contains("SELECT") && autoGeneratedSQLQuery.contains("FROM")) {
                    log.info("** Template SQL Query **  \n" + autoGeneratedSQLQuery);
                } else {
                    log.info("No SQL template query was generated.");
                }

                // WHERE clause might be missing if no data property is specified
                if (autoGeneratedSQLQuery.contains("SELECT") && autoGeneratedSQLQuery.contains("FROM")) {
                    log.info("======================================================================");
                    log.info("* Reformatting SQL Query Template to instantiate the input variables *");
                    log.info("======================================================================");
                    SQLTemplateReformatter sqlTemplateReformatter = new SQLTemplateReformatter();
                    String reformattedSQLQuery = sqlTemplateReformatter.reformatSQLTemplateQuery(autoGeneratedSQLQuery);
                    log.info("Reformatted SQL Query:  \n\t\t" + reformattedSQLQuery);
                } else {
                    log.info("No SQL Query template to reformat");
                }

                log.info("===========================================================");
                log.info("* Generating Input Java code from Input Class Expressions *");
                log.info("===========================================================");
                InputJavaCodeGenerator inputJavaCodeGenerator = new InputJavaCodeGenerator();
                // create java code only if there is any input datatype defined in the class expression 
                if (!varsForSQLWhereClause.isEmpty()) {
                    inputJavaCodeGenerator.generateInputJavaCode(inputClassExpr, inputGraph, varsForSQLWhereClause);
                    //ioJavaCodeGenerator.printStatements();
                    String generatedInputCodeBlock = inputJavaCodeGenerator.getAUTO_GENERATED_INPUT_CODE();
                    log.info(generatedInputCodeBlock);
                } else {
                    log.info("Not creating any code for the input class expression.");
                }

                log.info("============================================================");
                log.info("* Generating Output Java code from Input Class Expressions *");
                log.info("============================================================");
                OutputJavaCodeGenerator outputJavaCodeGenerator = new OutputJavaCodeGenerator();
                // create java code only if there is any input datatype defined in the class expression 
                if (!answerVariables.isEmpty()) {
                    outputJavaCodeGenerator.generateOutputJavaCode(outputClassExpr, outputGraph, answerVariables);

                    //String generatedOutputCodeBlock = outputJavaCodeGenerator.getAUTO_GENERATED_OUTPUT_CODE();
                    //log.info(generatedOutputCodeBlock);
                } else {
                    log.info("Not creating any code for the output class expression.");
                }

                log.info("=====================================================");
                log.info("* Generating Service Code in " + AUTO_GENERATED_SERVICE_DIR + "*");
                log.info("=====================================================");
                ServiceCodeGenerator codeGenerator = new ServiceCodeGenerator(serviceOntologyURI, serviceName, inputClassURI, outputClassURI, email, description);
                codeGenerator.generate();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String generateSQLQueryTemplate(String tptpQuery) {
        /*
        String predicates = "--p_is_performed_for(D,P),\n"
                + "    --p_has_first_name(P,\"John\"),\n"
                + "    --p_has_diagnosis_code(D,\"InputString\"),\n"
                + "\n"
                + "\n"
                + "    ++answer(P)";

        predicates = "--p_Patient(X),\n"
                + "    --p_has_first_name(X,N1),\n"
                + "    --p_has_last_name(X,N2),\n"
                + "    ++answer(N1,N2)";
        predicates = "--p_Patient(X),\n"
                + "    --p_has_first_name(X,N1),\n"
                //+ "    --p_has_last_name(\"X\",N2),\n"
                + "    ++answer(N1)";
        predicates = "--p_Patient(X),\n"
                + "    --p_has_first_name(X,N1),\n"
                + "    --p_has_last_name(X,N2),\n"
                + "    ++answer(N2)";
        predicates = "--p_Patient(entityForPatient(\"X0\")), --p_has_first_name(entityForPatient(\"X0\"), Y1), --p_has_last_name(entityForPatient(\"X0\"), Y3), ++answer(Y1, Y3)";

        String tptpQuery
                = "include('rdb2ont.tptp').\n"
                + "\n"
                //+ "% domain ontology translated into tptp fof formulas \n"
                //+ "include('HAI_no_Illegal_Symbols.ontology.fof.tptp').\n"
                + "include('HAI.owl.tptp').\n"
                + "\n"
                + "% semantic query\n"
                + "input_clause(query4patOacisPID,conjecture,\n"
                + "  [\n"
                + predicates
                + "  ]).";
        */
        log.info("--------------------------------");
        log.info("* Generating Schematic Answers *");
        log.info("--------------------------------");
        SchematicAnswersGenerator schematicAnswersGenerator = new SchematicAnswersGenerator(tptpQuery);
        String schematicAnswers = schematicAnswersGenerator.generateSchematicAnswers();

        System.out.println(schematicAnswers);

        SAILQueryManager sailQueryManager = new SAILQueryManager();

        /**
         * we set the args "--ecnomical-joins off --table-query-patterns
         * predicate_views.xml"
         */
        log.info("--------------------------------------");
        log.info("* Resolution steps during generation *");
        log.info("--------------------------------------");
        String[] args = new String[]{"--output-xml", "off", "--ecnomical-joins", "off", "--table-query-patterns", System.getProperty(JAVA_IO_TEMP_DIR_NAME).concat("/" + SQL_TEMPLATE_DIR_NAME) + "/" + PREDICATE_VIEWS_XML_FILENAME};
        String autoGeneratedSQLQuery = "";
        try {
            autoGeneratedSQLQuery = sailQueryManager.generateSQLTemplate(args, schematicAnswers);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return autoGeneratedSQLQuery;
    }

    public static void main(String[] args) throws Exception {

        //args = new String[]{"-s ", "http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getInsecticideIdByAssayId.owl", " -e ", "sadnanalamnir@gmail.com", " -d ", "TEST_DESCRIPTION", " --seed", " --generate"};
        boolean hasArguments = false;
        if (args.length != 0) {
            if (args.length == 1 && args[0].equals("${args}")) {
                hasArguments = false;
            } else {
                hasArguments = true;
            }
        }

        if (hasArguments) {
            String arguments = Arrays.toString(args).replace(", ", " ");
            arguments = arguments.substring(1, arguments.length() - 1);
            log.info("====================");
            log.info("* Arguments passed *");
            log.info("====================");
            log.info(arguments);
            run(arguments);
        } else {
            System.out.println("NO ARGUMENTS. CHECKARGUMENTS.");

        }
        System.out.println("\nAll done.");

    }
}
