package ca.unbsj.cbakerlab.foodweb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

/**
 *
 * @author UNBSJ
 */
public class Pipeline implements Serializable {

    private static final Logger log = Logger.getLogger(Pipeline.class);

    

    public Pipeline(String gateHome) {
        
    }

    public void init(boolean runDocumentResetter,
            boolean runTokenizer,
            boolean runSentenceSplitter,
            boolean runPosTagger,
            boolean runNpChunker,
            boolean runChemicalExtractor,
            boolean runDiseaseExtractor,
            boolean runCooccurrenceExtractor
    )  {

        
    }

    /**
     * Tell MutationGrounderApp's controller about the corpus you want to run on
     */
    public void setCorpus() {
        //annieController.setCorpus(corpus);
    }

    /**
     * Run MutationGrounderApp
     */
    public void execute(){
        log.info("Running Pipeline...");
        

        log.info("...Pipeline complete");
    }

}
