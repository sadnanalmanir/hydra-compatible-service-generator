package ca.unbsj.cbakerlab.foodweb;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;

public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    static long corpusQueryTime = 0;

    public Main() {

    }

    /**
     * Executed from main. Supports JSAP parser to read arguments from command
     * line. Details of how to use arguments in Main please read in readme
     *
     * @param args arguments from command line
     * @throws Exception
     */
    public static void run(String args) {

        try {
            long time = System.currentTimeMillis();

            // Get command line arguments.
            String serviceOntologyURI = null;            
            String serviceName = null;
            String inputClassURI = null;
            String outputClassURI = null;
            String email = null;
            String description = null;

		boolean needSeedInputData = false;
            boolean runGenerator = false;

            //
            // Define options.
            //
            {
                JSAP jsap = new JSAP();
                {
                    FlaggedOption s = new FlaggedOption("serviceOntologyURI").setStringParser(JSAP.STRING_PARSER).setLongFlag("Service Ontology URI").setRequired(true).setShortFlag('s');
                    s.setHelp("URI of the service ontology.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("email").setStringParser(JSAP.STRING_PARSER).setLongFlag("email").setRequired(true).setShortFlag('e');
                    s.setHelp("The contact email of the author.");
                    jsap.registerParameter(s);
                }

		    {
                    FlaggedOption s = new FlaggedOption("description").setStringParser(JSAP.STRING_PARSER).setLongFlag("description").setRequired(true).setShortFlag('d');
                    s.setHelp("The description of the sevice.");
                    jsap.registerParameter(s);
                }
                
                {
                    Switch s = new Switch("needSeedInputData").setLongFlag("seed");
                    s.setHelp("needSeedInputData: Service allPatients from a table do NOT need input values.");
                    jsap.registerParameter(s);
                }


                {
                    Switch s = new Switch("runGenerator").setLongFlag("generate");
                    s.setHelp("runGenerator");
                    jsap.registerParameter(s);
                }

                // Parse the command-line arguments.
                JSAPResult config = jsap.parse(args);

                // Display the helpful help in case trouble happens.
                if (!config.success()) {
                    System.err.println();
                    System.err.println(" " + jsap.getUsage());
                    System.err.println();
                    System.err.println(jsap.getHelp());
                    System.exit(1);
                }

		// Assign to variables.

                serviceOntologyURI = config.getString("serviceOntologyURI");
                if (serviceOntologyURI != null) {
                    log.info("Service Ontology URI: " + serviceOntologyURI);
                }
			
                email = config.getString("email");
                if (email != null) {
                    log.info("Contact email: " + email);
                }


                description = config.getString("description");
                if (description != null) {
                    log.info("Description: " + description);
                }

		    needSeedInputData = config.getBoolean("needSeedInputData");
                if (needSeedInputData) {
                    log.info("needSeedInputData [e.g. getXByY]: " + needSeedInputData);
                }
                // Modules.
                //
                runGenerator = config.getBoolean("runGenerator");
                if (runGenerator) {
                    log.info("runGenerator: " + runGenerator);
                }

                
            }

            
            //
            // RUN Generator.
            //	
            if (runGenerator) {

		/*
                Properties pro = new Properties();
                try {
                    pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                String gateHome = pro.getProperty("GATE_HOME");
		//
                // Init pipeline.
                //
                Pipeline pipeline = new Pipeline(gateHome);
                pipeline.init(
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true
                );
		
                log.info("Processing Corpus Time " + (System.currentTimeMillis() - time) / 1000 + " seconds");
                
                */
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {

        boolean hasArguments = false;
        if (args.length != 0) {
            if (args.length == 1 && args[0].equals("${args}")) {
                hasArguments = false;
            } else {
                hasArguments = true;
            }
        }

        if (hasArguments) {
            String arguments = Arrays.toString(args).replace(", ", " ");
            arguments = arguments.substring(1, arguments.length() - 1);
            log.info("ARGUMENTS: " + arguments);
            run(arguments);
        } else {
            System.out.println("NO ARGUMENTS. CHECKARGUMENTS.");

        }
        System.out.println("\nAll done.");

    }
}
